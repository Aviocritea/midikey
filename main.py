import os
import time as t
import pygame.midi
from pynput.keyboard import Controller, Key
import key_bindings

keyboard = Controller()

pygame.midi.init()

def midi_test() -> int:
	print('\nInput MIDI devices:')
	for i in range(pygame.midi.get_count()):
		info = pygame.midi.get_device_info(i)
		if info[2]:
			print(i, info[1].decode())
	device_num = int(input('Select MIDI device to test: '))

	print('Opening MIDI device:', device_num)
	m = pygame.midi.Input(device_num)
	try:
		m.poll()
	except:
		print("Couldn't open MIDI device")
	
	print('Device opened for testing. Use ctrl-c to quit.\n')
	#print(m.read(device_num))
	return m


def midi_read(mDevice):
	mdInfo = list(mDevice.read(1))
	# Works now but should be fixed later, device won't always be 1
	try:
		isPressed = mdInfo[0][0][2]
		keyPressed = mdInfo[0][0][1]
		t.sleep(0.04)

		return keyPressed, isPressed

	except:
		t.sleep(0.04)
		pass


def bind_keys(midiRead):
	currentDir = os.getcwd()
	r = True
	
	print()
	print("First, press the key on the MIDI keyboard you'd like to bind a letter to: ")

	#midiKeyToBind = 55
	
	while r == True:
		mr = midi_read(midiRead)
		#print(mr)   for debuging
		if mr != None:
			midiKeyToBind = mr[0]
			r = False

	print("Key chosen:", midiKeyToBind, "\n")

	qwertyKeyToBind = input("Next, hit the key on your computer keyboard you'd like it to represent: \n")

	key_bindings.kbDict[str(midiKeyToBind)] = qwertyKeyToBind
	print('\nKey "' + str(midiKeyToBind) + '" is now binded to "' + str(qwertyKeyToBind) + '" \n')

def keyboard_em(midiRead, kbDict):
	keyPressed = midiRead[0]
	isPressed = midiRead[1]

	print(keyPressed, isPressed)

	try:
		if keyPressed > 0:
			a = kbDict[keyPressed]
			if isPressed == 100:
				keyboard.press(a)

			elif isPressed == 0:
				keyboard.release(a)
	
	except:
		if kbDict[keyPressed] == None:
			pass

		if kbDict[keyPressed] == 'quit':
			#keyboard.press(Key.ctrl)
			pass

		if kbDict[keyPressed] == 'enter':
			keyboard.press(Key.enter)
			keyboard.release(Key.enter)

		if kbDict[keyPressed] == 'up':
			keyboard.press(Key.up)
			keyboard.release(Key.up)

		if kbDict[keyPressed] == 'down':
			keyboard.press(Key.down)
			keyboard.release(Key.down)

		if kbDict[keyPressed] == 'left':
			keyboard.press(Key.left)
			keyboard.release(Key.left)

		if kbDict[keyPressed] == 'right':
			keyboard.press(Key.right)
			keyboard.release(Key.right)


def main():
	# PMI means pygame.midi.input
	PMI = midi_test()
	makeKBs = True

	if 0:
		bk = input("Do you want to create a new keybinding? (y or n): \n")	
		
		while makeKBs == True:   	
			if bk == "y":
				bind_keys(PMI)

			elif bk == "n":
				makeKBs = False
				print("\nStarting MIDIKey now...")

			else:
				#print('invalid input, please enter "y" for yes, or "n" for no')
				print("Starting MIDIKey now...")

	print(key_bindings.kbDict)

	while True:
		mr = midi_read(PMI)
		if mr != None:
			keyboard_em(mr, key_bindings.kbDict)


if __name__ == "__main__":
	main()