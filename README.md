# MIDIKey

Use a MIDI keyboard as an alphanumeric keyboard

## Step 1: Install the latest version of Python

Go to python.org/downloads/ and download the most recent version of Python for your OS, alternitivly install Python 3.10._

## Step 2: Install the required packages

After you've installed Python, open Windows command prompt. Once there, enter the following commands:

'pip install pygame'
'pip install pynput'

After that you should be good to go.

## Step 3: Run the program

Navigate to "main.py" and run it.
